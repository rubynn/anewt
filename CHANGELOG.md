# Changelog

If I change something, I'll put it here. I started versioning before creating this file, created it at 1.2, and ANEWT was mostly done by then, so I don't think I'm gonna use this much.

## 1.3

- Added message when redirecting to dedicated translation page.
- Fixed shown URL when warning about no translation tables being present.
- Fixed translation tables not being updated when finding a dedicated translations page (tested using *tea*).
- Proved the opening paragraph of the changelog wrong.

## 1.3.1

- Removed ability to change language preferences from the title screen.
- Changing language preferences now clears the screen.
- Other code refactoring, including using `with` when opening the JSON file and adding `try` lines to catch possible errors when reading and writing the preferences file.

## 1.3.2

- Added notice to not contact me through Wikimedia on the title screen. Seriously, please do not contact me through Wikimedia for ***ANYTHING*** unrelated to Wikimedia projects.

## 1.3.3

- Fixed a crash when parsing a site with a single translation table (tested using *Uralic*, *toyboy*, and *rational number*)

## 2.0

- Added ability to save multiple language sets. It's not perfect, as it doesn't let you edit or delete them, but it works fine for my needs, and editing JSON files manually in case something messes up isn't too hard anyways.
- Changed `!changelangs` to `!ms` (short for Manage Sets)
- Better handling of pseudo-tables (those non-tables that are in the list of the rest of the translation tables, but just link to another entry's translation list). They're now listed aside, with a link to their translations.
- Collapsing some multi-line bits of code into single-line, longer bits.
- Reduced use of regular expressions in favor of other methods. Might make the code faster or slower, I have no idea, but it certainly makes the code it easier on the eyes.
- Minor optimizations, like using `or` instead of `if` when defining variables, and not defining variables that'll go unused.
- Other stuff I don't remember.
