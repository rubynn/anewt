# Automatic Narrowable English Wiktionary Translator

As the tin says, this is an easy-to-use English Wiktionary translator CLI app. It needs an internet connection to run.

This is my first """real""" programming project, but it was initially made just for a personal conlanging project, so don't expect anything big with tons of functionality. It's just two files, in fact, the Thing That Runs (anewt-source.py) and a JSON to save your language preferences (anewt-langsets.json).

## Installation

This uses Beautiful Soup 4 and Requests, so consider that if you want to run this directly from the .py file. Otherwise, you can just download the file and run it. You don't need to download the JSON file, the program will create one automatically if it doesn't find one.

If I end up figuring out how to release this thing as an executable (because GitLab makes this hard for my absurdly smooth brain), then you can download that too. Until then, you'll have to run it directly from the source file. Not that it's hard, just annoying.

Either way, make sure the program is on a directory it can write on, so it can create and save your language preferences JSON. It'll crash otherwise.

## Usage

On a fresh install, ANEWT should open to a title screen, which you can move from by pressing ENTER.

It'll detect there's no language preference file and proceed with creating one. It'll first ask for a name of your first language set, which is the group of languages you want to translate into. You can label this whatever you want, as long as it isn't empty, a duplicate, or `!x` (used across the app to mean "cancel"). It'll then prompt you to add the languages you want to the set. Make sure the names are spelled exactly the same as in English Wiktionary, including spaces and letter cases.

Afterwards, it'll ask you to input a word. It'll go to the English Wiktionary entry for that word and pull up the translation table(s) for it. If there's a dedicated translation page (like with *I*, *man*, *he*,...), it'll go there automatically. If there are multiple translation tables on a page (like with *run*, *flag*, *we*,...), it'll prompt you to choose one by inputting its index number (which is next to the table's name). If there's a dedicated translation page AND multiple translation tables (like with *dog*), it'll ask you how it should proceed. I've made the decision to not display the *Translations to be checked* table, as it's possible that these translations are wrong.

It'll then show you the translations for the word you chose in the languages you chose, taken straight from English Wiktionary. Currently, this includes the superscript text that would, on the site, link to the word's entry on a different Wiktionary, like (fr) for French Wiktionary or (sw) for Swahili Wiktionary. If it doesn't find any translations for your chosen languages, it'll just ask you if you want the entire table instead.

Rinse and repeat.

If you want to manage your language sets, you can input `!ms` when it asks you for a word to translate. Follow the on-screen instructions.

## Credits

Logo is *newt* by Georgiana Ionescu (CC-BY 3.0) from The Noun Project, with the font being Linux Libertine by Philipp H. Poll.

## License

This is project is licensed under the MIT license, which you can read in its entirety on the LICENSE.txt file.

## "Help, your code sucks and doesn't work!"

Not surprising. Try to whip your computer into submission. Or give it cookies, that one tends to go better.

There's also [Wikidata Translate](https://hay.toolforge.org/wdtranslate/) by Hay Kranen, which works on Wikidata instead of Wiktionary and has a graphical interface.
