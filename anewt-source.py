import requests, bs4, re, json, os, sys
from pathlib import Path

def get_bundle_dir():
  if getattr(sys, 'frozen', False): return Path(sys._MEIPASS)

  else: return Path(__file__).resolve().parent

setsDir = get_bundle_dir() / 'anewt-langsets.json'

def load_sets():
  if not os.path.exists(setsDir): return {'languages': {}}

  try:
    with setsDir.open('r') as file:
      return json.load(file)
    
  except json.JSONDecodeError:
    print("Error decoding preferences JSON file.")
    return {'languages': {}}

def list_sets(setsData):
  if not setsData['languages']:
    print("No language sets available.")
    return None

  print("\nAvailable language sets:")
  for i, (setLabel, languages) in enumerate(setsData['languages'].items()):
    print(f"{i}. {setLabel} - Languages: {', '.join(languages)}")

def create_set(setsData,firstTime=False):
  print("\nEnter a custom label for the language set.\n")
  if not firstTime: print("Type !x to cancel\n")

  while True:
    setLabel = input().strip()

    if setLabel == '!x':
      if firstTime:
        print("'!x' may not be used as a name.")
        continue
      return None
    
    elif not setLabel:
      print("Set must have a name.")
      continue

    elif setLabel in setsData['languages']:
      print(f"The set '{setLabel}' already exists.")
      continue

    break
    
  while True:
    langs = input("Input the languages you want to include in the set, separated by commas and written as they are in Wiktionary (e.g., Mandarin, Võro, Classical Nahuatl).\n").strip()

    if not langs:
      print("Set must have at least one language.")
      continue

    langs = sorted([lang.strip() for lang in langs.split(',')])
    setsData['languages'][setLabel] = langs

    try:
      with setsDir.open('w') as file:
        json.dump(setsData, file, indent=2)
      print(f"Language set '{setLabel}' updated.")
      return langs

    except IOError:
      input("Error writing preferences to file. Press ENTER to continue.")
      return None

def manage_sets(allowCancel=True):
  setsData = load_sets()

  if setsData == {'languages': {}} or setsData == None:
    print("Preferences file empty, nonexistent, unopenable. Proceeding with set creation...")
    return create_set(setsData,True)
  else:
    list_sets(setsData)

  while True:
    print("\nSelect a language set by its number to swap to it, or enter 'c' to create a new language set\n")

    if allowCancel: print("Type !x to cancel\n")
    action = input().strip().lower()

    if action.isdigit() and 0 <= int(action) < len(setsData["languages"].keys()):
      setLabel = list(setsData['languages'].keys())[int(action)]
      print(f"Selected language set: {setLabel}")
      return setsData['languages'][setLabel]
    
    if action == 'c':
      return create_set(setsData)
    
    if action == '!x' and allowCancel:
      return None
      
    print("Invalid selection. Try again.")

def get_soup(url):
  try:
    response = requests.get(url)
    response.raise_for_status()
    return bs4.BeautifulSoup(response.text, "html.parser")
  
  except requests.RequestException as e:
    print(f"Error fetching URL: {e}")
    return None

def find_tables(soup):
  tables, pseudoTables = [], []

  for div in soup.find_all('div', id=lambda x: x and x.startswith('Translations-')):
    (tables if div.find('div', class_='NavContent') else pseudoTables).append(div)

  return tables, pseudoTables

def list_tables(tables, word, skip=False):
  if not skip:
    print(f"Found {len(tables)} translation tables for '{word}':")

  for index, table in enumerate(tables):
    print(f"  {index} - {table.text.splitlines()[0]}")

def choose_table(tables):
  while True:
    tableChoice = input("\nEnter the number of the table you want to see, or 'q' to quit: ").strip().lower()

    if tableChoice == 'q':
      return None
    
    elif tableChoice.isdigit() and 0 <= int(tableChoice) < len(tables):
      return int(tableChoice)
    
    print("Invalid input. Try again.")

def handle_dedicated_page(word, skip=False):
  if not skip:
    print("Found dedicated translation page. Requesting it...")
  newSoup = get_soup(f"https://en.wiktionary.org/wiki/{word}/translations")
  tables, pseudoTables = find_tables(newSoup)

  if len(tables) > 1:
    list_tables(tables, word)
    tableChoice = choose_table(tables)

  else:
    tableChoice = 0

  return tableChoice, tables, pseudoTables

def pseudotables_warning(pseudoTables):
  if pseudoTables: print("The following senses point to another entry, in another URL, for translations:")

  for pseudoTable in pseudoTables:
    print(f"  • {pseudoTable.text.strip()} (https://en.wiktionary.org{pseudoTable.find('a')['href']})")

  print()

def handle_fork(word, tables):
  print("Found a dedicated translation page AND the following translation tables:")
  list_tables(tables, word, True)

  while True:
    forkChoice = input("Type 'D' to go to dedicated translation page. Type a table's index number to process that table.\n").strip().lower()

    if forkChoice == 'd':
      print("Going to dedicated translation page...")
      tableChoice, tables, pseudoTables = handle_dedicated_page(word,True)
      return tableChoice, tables, pseudoTables
    
    if forkChoice.isdigit() and 0 <= int(forkChoice) < len(tables):
      return int(forkChoice), tables, None
    
    print("Invalid input. Try again.")

def enwikt_translate(word,langs):
  url = f"https://en.wiktionary.org/wiki/{word}"
  soup = get_soup(url)

  if not soup:
    print("Failed to retrieve or parse the webpage. Please check the provided word or your internet connection.")
    return
  
  tables, pseudoTables = find_tables(soup)
  if pseudoTables: pseudotables_warning(pseudoTables)
    
  tableStatus = 0b0
  if soup.find_all("a", attrs={"href": re.compile(r"/translations")}): tableStatus += 0b1
  if len(tables) > 1: tableStatus += 0b10
    
  # tableStatus = 0b00 (0) is a single table in page (e.g., "Uralic")
  #      "      = 0b01 (1) is a page with a dedicated translation page (e.g., "I")
  #      "      = 0b10 (2) is a page with multiple tables (e.g., "flag")
  #      "      = 0b11 (3) is a page with multiple tables AND a dedicated translation page (e.g. "water")

  match tableStatus:
    case 0b0:
      tableChoice = 0
      
    case 0b1:
      tableChoice, tables, pseudoTables = handle_dedicated_page(word)
      if pseudoTables: pseudotables_warning(pseudoTables)

    case 0b10:
      list_tables(tables, word)
      tableChoice = choose_table(tables)

    case 0b11:
      tableChoice, tables, pseudoTables = handle_fork(word, tables)
      if pseudoTables: pseudotables_warning(pseudoTables)

    case _:
      print("ERROR: tableStatus has returned a value outside of 0b0 to 0b11")
      return
  
  if not tables:
    print(f"No translation tables found at {url}. Non-english-language words, misspellings, alternate spellings, inflections, and perfect synonyms do not get translations tables (see https://en.wiktionary.org/wiki/Wiktionary:Translations).")
    return
  
  langsPattern = '|'.join(map(re.escape, langs))
  tablePattern = re.compile(rf"^({langsPattern}):\s*(.*)$", re.MULTILINE)
  matches = tablePattern.findall(tables[tableChoice].text)

  if not matches:
    while (printAllChoice := input("No translations for chosen languages found. Do you want to print the entire table instead? (y/n)\n").lower()) not in ['y', 'n']:
      print("ERROR: Please type y or n to make your choice.\n")
    
    if printAllChoice == 'y':
      print(tables[tableChoice].text)
      
  else:
    print(f"\nTable: {tables[tableChoice].text.splitlines()[0]}\n----------------------------------")
    for translation in matches: print(f"{translation[0]}: {translation[1]}")

print('''
                    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
                 ▄▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄▄▄▄▄▄▄
               ▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄▄▄
              █▒▒▒▒▒▒▒▒███▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄▄▄
             █▒▒▒▒▒▒▒▒█   █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄
             █▒▒▒▒▒▒▒▒█   █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄
              █▒▒▒▒▒▒▒▒███▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄
               █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄
                ▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                   ▀▀▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                        ▀▀▀▀▀▀▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒█ █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄
                                 ▀▀█▒▒▒▒▒▒▒▒▒▒█  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                  █▒▒▒▒▒▒▒▒▒▒▒█  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                 █▒▒▒▒▒▒▒▒▒▒▒▒█  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀▀▀▀▀█▒▒▒▒▒▒▒▒▒▒▒▒█
                                  █▒▒▒▒▒▒▒▒▒█▀   █▒▒▒▒▒▒▒▒▒▒▒▒█▀  ▄▄▄▄▄██▒▒▒▒▒▒▒▒▒▒▒█
                                   ▀▀▀▀▀▀▀▀▀      ▀▀▀█▒▒▒▒▒▒▒▒█  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                      ▀▀▀▀▀▀▀▀  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                       ▄▄▄▄▄▄▄▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                      █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                      █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                      █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                       ▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                           ▄▄▄▄▄▄▄       ▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                          █▒▒▒▒▒▒▒█        ▀▀▀▀▀▀▀▀▀▀▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                          █▒▒▒▒▒▒▒▒█▄                  █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                           █▒▒▒▒▒▒▒▒▒█▄                █▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                           █▒▒▒▒▒▒▒▒▒▒▒█▄             ▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                            █▒▒▒▒▒▒▒▒▒▒▒▒█▄▄▄▄    ▄▄▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                             ▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                               █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                ▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                  ▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█
                                                    █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀▀
                                                     ▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀
                                                        ▀▀▀▀█▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀
                                                             ▀▀▀▀▀▀▀▀▀▀▀▀▀▀''')

print('ANEWT - Automatic Narrowable English Wiktionary Translator - v2.0'.center(88))
print("by ýñ (User:Rubýñ on Wikimedia. Please don't contact me through there)\n".center(88))
print('Logo is "newt" by Georgiana Ionescu (CC-BY 3.0) from The Noun Project.\n'.center(88))
print("You can press CTRL+C at any point quit, unless you have text highlighted.".center(88))
print("(This is a Python thing, not my decision.)\n".center(88))
print("If I crash, check if I'm on a directory I'm allowed to write on.\n".center(88))

input("Press ENTER to continue.\n".center(88))
os.system('cls')

langs = manage_sets(False)
os.system('cls')

while True:
  word = input(f"Input an English Wiktionary entry to translate into: {', '.join(langs)}.\nInput '!ms' to manage your translation language sets or '!x' to quit.\n").strip().lower()
  
  if not word:
    print("Empty input. Please enter a valid word.")
    continue

  if word == '!ms':
    os.system('cls')
    langs = manage_sets() or langs
    os.system('cls')
    continue

  if word == '!x':
    break

  enwikt_translate(word, langs)

  if input("\nPress ENTER to translate again. This will clear the screen.\nType '!x' (in any lettercase) and then press ENTER to quit program.\n").strip().lower() == "!x":
    break
  
  os.system('cls')
